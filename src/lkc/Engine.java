package lkc;

import java.io.IOException;
import java.util.Random;
import java.util.Stack;

import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.game.GameCanvas;

import lkc.control.WormSteering;
import lkc.display.DisplayObject;
import lkc.display.DrawTools;
import lkc.display.QuadPainter;
import lkc.display.TexturedQuad;
import lkc.mato.Pallo;
import lkc.mato.Mato;

import com.nokia.mid.ui.multipointtouch.MultipointTouch;
import com.nokia.mid.ui.multipointtouch.MultipointTouchListener;


public class Engine extends GameCanvas implements MultipointTouchListener,
		Runnable {

	protected Engine(boolean suppressKeyEvents) {
		super(suppressKeyEvents);
	}

	private Stack objects = new Stack();
	private int bg_color = 0;// 0x88000000;
	private QuadPainter qp;
	private WormSteering ws;
	private Pallo ball;

	private long fpst = 0;
	private float fps = 0;
	private long fpsc = 0;

	private MultipointTouch mp = MultipointTouch.getInstance();

	private Mato mato1;
	private Mato mato2;
	static private Random random = new Random();
	int p1points;
	int p2points;
	
	public void start() {
		mp.addMultipointTouchListener(this);
		qp = new QuadPainter();
		WormSteering ws1 = new WormSteering((short)-120, (short)-128, (short)240, (short)32, true, qp);
		WormSteering ws2 = new WormSteering((short)-120, (short)160, (short)240, (short)32, false, qp);
		mato1 = new Mato(qp, (float) Math.PI/2, 4.5f, new Vector2f(0, -128), 0x0000cc, ws1);
		mato2 = new Mato(qp, (float) -Math.PI/2, 4.5f, new Vector2f(0, 128), 0xcc0000, ws2);
		try {
			ball = new Pallo(Image.createImage("/pallo.png"), 0.001f);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ball.setPos(0, 0);
		ball.setVel(new Vector2f(0.54f, -1.8f));
		objects.addElement(ball);
		this.setFullScreenMode(true);
		fpst = System.currentTimeMillis();
		for(int i=0; i<10; i++){
			mato1.grow();
		}
		for(int i=0; i<10; i++){
			mato2.grow();
		}
		new Thread(this).start();
	}

	public static Random getRandom(){return random;}
	
	public void paint(Graphics g) {
		g.setColor(bg_color);

		if (qp != null) {
			qp.paint(g);
		}
		if (mato1 != null){
			mato1.move(ball);
		}
		if (mato2 != null){
			mato2.move(ball);
		}
		for (int i = 0; i < objects.size(); i++) {
			((DisplayObject) objects.elementAt(i)).paint(g);
		}
		g.setColor(0xFF0000);
		g.drawString("fps: " + fps, 0, 0, Graphics.TOP | Graphics.LEFT);
		if (fpsc == 60) {
			fps = 1000 / ((System.currentTimeMillis() - fpst) / 60.0f);
			fpst = System.currentTimeMillis();
			fpsc = 0;
		}
		g.drawString(p1points+"-"+p2points, 0, 160, Graphics.TOP | Graphics.LEFT);
		fpsc++;
		if (ball != null)
			for (int i = 0; i < 4; i++){
				if (ball.move()){
					if (ball.getPy() > 0){
						p1points++;
						mato1.grow();
						mato1.grow();
						mato1.grow();
						mato1.speed += 0.5;
					}
					else{
						p2points++;
						mato2.grow();
						mato2.grow();
						mato2.grow();
						mato2.speed += 0.5;
					}
				}
			}
	}

	public void addChild(DisplayObject p) {
		objects.addElement(p);
	}

	public void removeChild(DisplayObject p) {
		objects.removeElement(p);
	}

	public void removeChildren() {
		objects.removeAllElements();
	}

	public void setBackground(int col) {
		bg_color = col;
	}

	public int getBackground() {
		return bg_color;
	}

	public void pointersChanged(int[] pids) {
		int pid;
		int x;
		int y;
		int state;

		for (int i = 0; i < pids.length; i++) {
			pid = pids[i];

			state = MultipointTouch.getState(pid);

			x = MultipointTouch.getX(pid);
			y = MultipointTouch.getY(pid);

			switch (state) {
			case MultipointTouch.POINTER_PRESSED:
				if (y > 160)
					mato1.wheel.touch(x, y);
				else
					mato2.wheel.touch(x, y);
				break;
			case MultipointTouch.POINTER_DRAGGED:
				if (y > 160)
					mato1.wheel.touch(x, y);
				else
					mato2.wheel.touch(x, y);
				break;
			case MultipointTouch.POINTER_RELEASED:
				if (y > 160)
					mato1.wheel.reset();
				else
					mato2.wheel.reset();
				break;
			default:
				break;
			}

		}

	}

	public void run() {
		while (true) {
			long end = (long) (System.currentTimeMillis() + 1000.0 / 60);
			Graphics g = this.getGraphics();
			this.paint(g);
			this.flushGraphics();
			try {
				Thread.sleep((Math.max(0, end - System.currentTimeMillis())));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
