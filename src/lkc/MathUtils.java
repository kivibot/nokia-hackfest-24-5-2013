package lkc;

public class MathUtils {
	static public double[][] tanTable = new double[91][2];
	static public double[][] cosTable = new double[91][2];
	static public double[][] sinTable = new double[91][2];
	
    static public void fillTanTable () {
        for (int i = 0; i < 91; i ++) {
            tanTable[i][0] = i*(Math.PI/180.0);
            tanTable[i][1] = Math.tan(i*(Math.PI/180.0));
        }
    }
    
    static public void fillCosTable () {
        for (int i = 0; i < 90; i ++) {
            cosTable[i][0] = i*(Math.PI/180.0);
            cosTable[i][1] = Math.cos(i*(Math.PI/180.0));
        }
        cosTable[90][0] = Math.PI/2;
        cosTable[90][1] = 0;
    }
    
    static public void fillSinTable () {
        for (int i = 0; i < 91; i ++) {
            sinTable[i][0] = i*(Math.PI/180.0);
            sinTable[i][1] = Math.sin(i*(Math.PI/180.0));
        }
        
    }
    
    static public double atan (double a) {
        double prev = 1;
        boolean neg = false;
        if (a<0){
        	neg = true;
        	a = -a;
        }
        for (int i = 0; i < 90; i++) {
            if (tanTable[i][1] == a) {
                return neg?-tanTable[i][0]:tanTable[i][0];
            }
            if (prev < a && tanTable[i][1] > a) {
                return neg?-tanTable[i][0]:tanTable[i][0];
            }
            prev = tanTable[i][1];
        }
        return 0;
    }
    
    static public float atan2 (float x, float y) {
        if (x == 0)
        	return y>0?Float.POSITIVE_INFINITY:Float.NEGATIVE_INFINITY;
        float asd = (float) atan(y/x);
        if (x<0)
        	asd += asd<0?Math.PI:-Math.PI;
        return asd;
    }
    
    static public double tan(double a){
    	double prev = 0;
    	while (a<=-Math.PI)
    		a+=Math.PI*2;
    	while (a>Math.PI)
    		a-=Math.PI*2;
    	if (a >= Math.PI/2)
    		a -= Math.PI/2;
    	if (a <= -Math.PI/2)
    		a += Math.PI/2;
    	boolean neg = false;
    	if (a < 0){
    		a = -a;
    		neg = true;
    	}
        for (int i = 0; i < 90; i++) {
            if (tanTable[i][0] == a) {
                return neg?-tanTable[i][1]:tanTable[i][1];
            }
            if (prev < a && tanTable[i][0] > a) {
            	return neg?-tanTable[i][1]:tanTable[i][1];
            }
            prev = tanTable[i][0];
        }
        return 0;
    }
    
    static public double cos(double a){
    	while (a<=-Math.PI)
    		a+=Math.PI*2;
    	while (a>Math.PI)
    		a-=Math.PI*2;
    	a = a<0?-a:a;
    	boolean neg = a > Math.PI/2;
    	if (neg){
    		a -= Math.PI;
    		a = -a;
    	}
        for (int i = 0; i < 91; i++) {
            if (cosTable[i][0] >= a) {
            	return neg?-cosTable[i][1]:cosTable[i][1];
            }
        }
        return 0;
    }
    
    static public double sin(double a){
    	while (a<=-Math.PI)
    		a+=Math.PI*2;
    	while (a>Math.PI)
    		a-=Math.PI*2;
    	boolean neg = a<0;
    	if (a > Math.PI/2){
    		a -= Math.PI;
    	}
    	if (a <= -Math.PI/2){
    		a += Math.PI;
    	}
    	a = a<0?-a:a;
        for (int i = 0; i < 91; i++) {
            if (sinTable[i][0] >= a) {
            	return neg?-sinTable[i][1]:sinTable[i][1];
            }
        }
        return 0;
    }
}