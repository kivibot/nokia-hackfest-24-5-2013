package lkc;

import javax.microedition.lcdui.Display;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;

public class MatoPong extends MIDlet{

	private Engine en;
	private Thread t;
	
	public MatoPong() {
		MathUtils.fillTanTable();
		MathUtils.fillCosTable();
		MathUtils.fillSinTable();
		en = new Engine(false);
	}

	public Engine getEngine() {return en;}
	
	protected void destroyApp(boolean unconditional)
			throws MIDletStateChangeException {

	}

	protected void pauseApp() {

	}

	protected void startApp() throws MIDletStateChangeException {
		Display.getDisplay(this).setCurrent(en);
		en.start();
	}

}
