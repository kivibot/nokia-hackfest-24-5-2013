package lkc;

public class Vector2f {
	
	public static final Vector2f ZERO = new Vector2f(0,0);
	
	public float x, y;

    public Vector2f(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public Vector2f add(float x, float y) {
        return new Vector2f(this.x+x,this.y+y);
    }

    public Vector2f add(Vector2f v) {
        return add(v.x, v.y);
    }
    
    public Vector2f substract(Vector2f v) {
        return add(-v.x, -v.y);
    }

    public float dot(Vector2f v) {
        return v.x * this.x + v.y * this.y;
    }

    public Vector2f multiply(float f){
    	return new Vector2f(x*f,y*f);
    }
    
    public float length() {
        return (float) Math.sqrt(x * x + y * y);
    }
    
    public float lengthSq() {
        return x * x + y * y;
    }

    public Vector2f normal() {
        return new Vector2f(y, -x);
    }
    
    public float distSq(Vector2f v) {
    	Vector2f a = substract(v);
    	return a.x * a.x + a.y * a.y;
    }
    
    public Vector2f reflect(Vector2f v) {
    	if (isZero())
    		return Vector2f.ZERO;
    	float l = length();
    	normalize();
    	v = v.normalize2().multiply(-1);
    	return (v.multiply(2)).substract(this).normalize2().multiply(l);
    }

    public void normalize() {
        float l = 1.0f / length();
        x *= l;
        y *= l;
    }
    
    public Vector2f normalize2() {
    	normalize();
        return this;
    }
    
    public float angle(){
    	return MathUtils.atan2(x, y);
    }
    
    public static Vector2f makeVec(float angle){
    	return new Vector2f((float) MathUtils.cos(angle),(float) MathUtils.sin(angle));
    }
    
    public String toString(){
    	return "("+x+", "+y+")";
    }
    
    public Vector2f clone(){
    	return new Vector2f(x, y);
    }

	public boolean isZero() {
		return x==0&y==0;
	}
}
