package lkc.control;

import java.io.IOException;

import lkc.display.QuadPainter;
import lkc.display.TexturedQuad;

public class WormSteering {
	float value;
	TexturedQuad rarrow;
	TexturedQuad larrow;
	TexturedQuad bg;
	int controlX;
	int x;
	int y;
	int width;
	int height;
	boolean gettingTouched;
	boolean mirror;
	
	public WormSteering(short x, short y, short width, short height, boolean inverted, QuadPainter qp){
		try {
			rarrow = new TexturedQuad("/nuolir.png", new Integer(0xFFFFFFFF), new short[]{(short) (x+width),y,0, (short) (x+width/2),y,0, (short) (x+width),(short) (y-height),0, (short) (x+width/2),(short) (y-height),0});
			larrow = new TexturedQuad("/nuolil.png", new Integer(0xFFFFFFFF), new short[]{(short) (x+width/2),y,0, (short) (x),y,0, (short) (x+width/2),(short) (y-height),0, (short) (x),(short) (y-height),0});
			bg = new TexturedQuad("/bg.png", new Integer(0xFFFFFFFF), new short[]{(short) (x+width),y,0, x,y,0, (short) (x+width),(short) (y-height),0, x,(short) (y-height),0});
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		qp.addQuad(bg);
		qp.addQuad(rarrow);
		qp.addQuad(larrow);
		this.width = width;
		reset();
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		mirror = inverted;
	}
	
	public void reset(){
		gettingTouched = false;
		value = 0;
		moveArrow();
	}
	
	public void moveArrow(){
		bg.resetTransform().postTranslate(0, 0, 1f);
		rarrow.resetTransform().postScale(value, 1, 1);
		rarrow.getTransform().postTranslate(0, 0, 1f);
		larrow.resetTransform().postScale(-value, 1, 1);
		larrow.getTransform().postTranslate(0, 0, 1f);
	}
	
	public float getValue(){
		return mirror?-value:value;
	}
	
	public boolean touch(int ttx, int tty){
		int tx = ttx - 120;
		int ty = 160 - tty;
		if (!(tx < x | tx > x+width | ty > y | ty < y-height))
			gettingTouched = true;
		if (gettingTouched){
			value = tx/(float)width*2;
			moveArrow();
		}
		return gettingTouched;
	}
}
