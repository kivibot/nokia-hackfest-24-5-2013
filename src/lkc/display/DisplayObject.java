package lkc.display;

import javax.microedition.lcdui.Graphics;


/**
 * 
 * Grafiikkojen abstrakti perusluokka ja ainoa luokka jonka Engine hyväksyy grafiikkana
 * 
 * @author kivi
 *
 */
public abstract class DisplayObject {
	
	/**
	 * Täällä tapahtuu kaikki piirtäminen
	 * @param g
	 * @see lkc.Engine
	 */
	public abstract void paint(Graphics g);
	
}
