package lkc.display;

import java.util.Stack;

import javax.microedition.lcdui.Graphics;

public class DrawTools {
	private static Stack clips = new Stack();

	public static final double sin45 = Math.sin(Math.PI / 4);

	public static void pushClip(Graphics g) {
		clips.push(new int[] { g.getClipX(), g.getClipY(), g.getClipWidth(),
				g.getClipY() });
	}

	public static void popClip(Graphics g) {
		int[] a = (int[]) clips.pop();
		g.setClip(a[0], a[1], a[2], a[3]);
	}

	public static void fillLineRect(Graphics g, int color, int x, int y, int w,
			int h, int linew, int skip) {
		pushClip(g);
		g.setColor(color);
		g.setClip(x, y, w, h);
		for (int i = -w; i < w; i += skip + linew + 1) {
			for (int j = 0; j < linew; j++) {
				g.drawLine(x + i + j, y, (int) (x + i + j + h * sin45), y + h);
			}
		}
		popClip(g);
	}
}
