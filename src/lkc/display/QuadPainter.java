package lkc.display;

import java.util.Random;
import java.util.Vector;

import javax.microedition.lcdui.Graphics;
import javax.microedition.m3g.Background;
import javax.microedition.m3g.Camera;
import javax.microedition.m3g.Graphics3D;
import javax.microedition.m3g.Light;
import javax.microedition.m3g.Transform;


public class QuadPainter extends DisplayObject {
	Vector texQuads;
	Camera c;
	Background bg;
	Light light;
	Graphics3D g3d = Graphics3D.getInstance();
	
	public QuadPainter(){
		texQuads = new Vector();
		bg = new Background();
		bg.setColor(0x005500);
		c = new Camera();
		c.setPerspective( 60.0f,              // field of view
	            (float)240 / (float) 320,  // aspectRatio
	            1.0f,      // near clipping plane
	            1000.0f ); // far clipping plane
		light = new Light();
		light.setMode( Light.AMBIENT );
		light.setColor(0xFFFFFF);
		light.setIntensity(10.f);
	}
	
	public void addQuad(TexturedQuad q) {
		texQuads.addElement(q);
	}
	
	public void removeQuad(Object q){
		while (texQuads.contains(q)) {texQuads.removeElement(q);}
	}
	
	public void removeQuadAt(int i){
		texQuads.removeElementAt(i);
	}

	public void paint(Graphics g) {
		g3d.bindTarget(g);
		g3d.clear(bg);
		g3d.setCamera(c, new Transform());
		g3d.resetLights();
		g3d.addLight(light, new Transform());
		for (int i = 0; i < texQuads.size(); i++){
			((TexturedQuad)(texQuads.elementAt(i))).paint(g3d);
		}
		g3d.releaseTarget();
	}
}
