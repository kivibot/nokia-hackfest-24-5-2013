package lkc.display;

import java.io.IOException;
import java.util.Vector;

import javax.microedition.lcdui.Image;
import javax.microedition.m3g.Appearance;
import javax.microedition.m3g.Graphics3D;
import javax.microedition.m3g.Image2D;
import javax.microedition.m3g.Material;
import javax.microedition.m3g.Texture2D;
import javax.microedition.m3g.Transform;
import javax.microedition.m3g.TriangleStripArray;
import javax.microedition.m3g.VertexArray;
import javax.microedition.m3g.VertexBuffer;

public class TexturedQuad {

	static private Vector paths = new Vector();
	static private Vector colors = new Vector();
	static private Vector appearances = new Vector();
	private Texture2D t2d;
	private VertexBuffer qdsVertexBuffer;
	private TriangleStripArray qdsIndexBuffer;
	private Appearance ap;
	private Transform tr;
	/* 2--1
	 * |\ |
	 * | \|
	 * 4--3
	 * */
	
	public void printQ(short[] q){
		String asd = "";
		for (int i = 0; i < q.length; i++){
			asd += "("+q[i]+")";
		}
		System.out.println(asd);
	}
	
	public int indexOf(Vector v1, Vector v2, Object o1, Object o2){
		for (int i = 0; i < v1.size(); i++){
			if (v1.elementAt(i).equals(o1) & v2.elementAt(i).equals(o2))
				return i;
		}
		return -1;
	}
	
	public TexturedQuad(String imgpath, Integer color, short[] quads) throws IOException {
		if (!paths.contains(imgpath) | !colors.contains(color)) {
			Image img = Image.createImage(imgpath);
			Image2D i2d = new Image2D(Image2D.RGBA, img);
			t2d = new Texture2D(i2d);
			i2d = null;
			t2d.setWrapping(Texture2D.WRAP_CLAMP, Texture2D.WRAP_CLAMP);
			t2d.setBlending(Texture2D.FUNC_MODULATE); 
			Material material = new Material();
	        try{
	            //set color for Material object. if 
	        	material.setColor(Material.AMBIENT, color.intValue());
	            material.setColor(Material.DIFFUSE, 0xFFFFFFFF); 
	            material.setColor(Material.SPECULAR, 0xFFFFFFFF);
	            material.setShininess(100.f);
	        }catch(Exception e) { 
	            //TODO: write handler code
	        }
			ap = new Appearance();
			ap.setTexture(0, t2d);
			ap.setMaterial(material);
			paths.addElement(imgpath);
			appearances.addElement(ap);
			colors.addElement(color);
		} else {
			ap = (Appearance) appearances.elementAt(indexOf(paths, colors, imgpath, color));
		}
		VertexArray qdsVertArray = new VertexArray( ( quads.length / 3 ), 3, 2 );
		qdsVertArray.set( 0, ( quads.length / 3 ), quads );
		
		byte[] qdsNorm = { 0, 0, 127,    0, 0, 127,    0, 0, 127,    0, 0, 127 };
		
		VertexArray qdsNormArray = new VertexArray( ( qdsNorm.length / 3 ), 3, 1 );
		qdsNormArray.set( 0, ( qdsNorm.length / 3 ), qdsNorm );

		short[] qdsTexcoord = { 1, 0,    0, 0,    1, 1,    0, 1 };
		
		VertexArray qdsTexArray = new VertexArray( ( qdsTexcoord.length / 2 ), 2, 2 );
		qdsTexArray.set( 0, ( qdsTexcoord.length / 2 ), qdsTexcoord );
		
		qdsVertexBuffer = new VertexBuffer();
		qdsVertexBuffer.setPositions( qdsVertArray, 1.f, null );
		qdsVertexBuffer.setNormals( qdsNormArray );
		qdsVertexBuffer.setTexCoords( 0, qdsTexArray, 1.f, null );
	
		int[] qdsStripLen = { 4 };
		qdsIndexBuffer = new TriangleStripArray( 0, qdsStripLen );
		
		
		
		
		tr = new Transform();
		tr.postTranslate(0, -1, -280);
		System.gc();
	}
	
	public TexturedQuad(String imgpath,  short[] quads) throws IOException {
		this(imgpath, new Integer(0xFFFFFF), quads);
	}

	public Transform getTransform(){
		return tr;
	}
	
	public Transform resetTransform(){
		tr = new Transform();
		tr.postTranslate(0, -1, -280);
		return tr;
	}
	
	public void paint(Graphics3D g3d) {
		g3d.render( qdsVertexBuffer, qdsIndexBuffer, ap, tr );
	}
}
