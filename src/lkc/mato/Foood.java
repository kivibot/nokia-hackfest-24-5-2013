package lkc.mato;

import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.game.Sprite;

import lkc.display.DisplayObject;

public class Foood extends DisplayObject{
	
	public float x,y,r;
	public Image img;

	
	public Foood(float x, float y, float r, Image food){
		this.x = x;
		this.y = y;
		this.r = r;
		img = food;
	}


	public void paint(Graphics g) {
		g.drawImage(img,(int)(x+120-r), (int)(160-y-r), Graphics.TOP|Graphics.LEFT);		
	}
	
	
	
}
