package lkc.mato;

import lkc.Vector2f;
import lkc.control.WormSteering;
import lkc.display.QuadPainter;

public class Mato {

	static float leftEdgeX = -120, rightEdgeX = 120, topY = 128,
			bottomY = -128;
	public MatoBlock head;

	public WormSteering wheel;
	QuadPainter qp;
	public float speed = 1;
	int length;
	float dir;
	int inc;
	int color;

	public Mato(QuadPainter qp, float dir, float speed, Vector2f pos, int color, WormSteering wh) {
		wheel = wh;
		this.qp = qp;
		this.dir = dir;
		this.speed = speed;
		this.color = color;
		head = new MatoBlock(null, pos, Vector2f.makeVec(dir).multiply(speed), false, null, color);
	}

	public static MatoBlock numPrev(int in, MatoBlock b){
		MatoBlock a = b;
		for (int i = 0; i < in; i++){
			if (a.prev == null)
				return null;
			a = a.prev;
		}
		return a;
	}
	
	public void grow() {
		inc = inc > length/8 ? 0 : inc;
		dir += wheel.getValue() * wheel.getValue() * wheel.getValue()
				/ (8.0 / speed);
		dir = (float) (dir <= -Math.PI ? dir + Math.PI * 2
				: dir > Math.PI ? dir - Math.PI * 2 : dir);
		MatoBlock n = new MatoBlock(head, head.pos.add(head.dir), Vector2f
				.makeVec(dir).multiply(speed), true, numPrev(length/8, head), color);
		head = n;
		if (inc != 0){
			qp.removeQuad(numPrev(length/10, head).tq);
		}
		if (n.tq != null)
			qp.addQuad(n.tq);
		inc++;
		length++;
	}

	public void move(Pallo p) {
		grow();
		MatoBlock n = head;
		while (n.prev != null) {
			if (n.prev.prev == null) {
				if (n.tq != null)
					qp.removeQuad(n.tq);
				n.prev = null;
				length--;
				break;
			}
			n = n.prev;
		}

		float px = head.pos.x, py = head.pos.y;
		boolean turned = false;
		Vector2f v = head.dir.clone();
		if (px < leftEdgeX & v.x < 0) {
			v.x = -v.x;
			turned = true;
		} else if (px > rightEdgeX & v.x > 0) {
			v.x = -v.x;
			turned = true;
		}
		if (py > topY & v.y > 0) {
			v.y = -v.y;
			turned = true;
		} else if (py < bottomY & v.y < 0) {
			v.y = -v.y;
			turned = true;
		}
		if (turned) {
			dir = v.angle();
		}
		collBall(p);
	}
	
	public MatoBlock[] getClosestBlocks(Pallo p){
		MatoBlock n = head;
		MatoBlock closest = head;
		float d = n.pos.distSq(new Vector2f(p.px, p.py));
		while (n.prev != null) {
			n = n.prev;
			float newd = n.pos.distSq(new Vector2f(p.px, p.py));
			if (newd < d){
				closest = n;
				d = newd;
			}
		}
		if (closest.equals(head)){
			return new MatoBlock[] {head};
		}
		if (closest.prev == null){
			return new MatoBlock[] {closest};
		}
		n = head;
		while (n.prev != closest) {
			n = n.prev;
		}
		boolean asd = n.pos.distSq(new Vector2f(p.px, p.py))<n.prev.prev.pos.distSq(new Vector2f(p.px, p.py));
		return new MatoBlock[] {asd?n:n.prev, asd?n.prev:n.prev.prev};
	}
	
	public void collBall(Pallo p){
		MatoBlock[] blocks = getClosestBlocks(p);
		if (blocks.length == 1){
			if (p.r*p.r > blocks[0].pos.distSq(new Vector2f(p.px, p.py))){
				if (blocks[0].equals(head)){
					 p.v = p.v.reflect(blocks[0].pos.substract(new Vector2f(p.px, p.py)).normal());
					 p.v = p.v.add(blocks[0].dir).multiply(0.7f);
				}
				else{
					p.v = p.v.reflect(blocks[0].pos.substract(new Vector2f(p.px, p.py)).normal());
				}
			}
		}/*
		else if (ballCollides(new float[] {p.px, p.py}, new float[] {blocks[0].pos.x, blocks[0].pos.y}, new float[] {blocks[1].pos.x, blocks[1].pos.y}, p.r, MatoBlock.r)) {
			System.out.println(System.currentTimeMillis()+"asdasd");
			p.v = p.v.reflect(blocks[0].pos.substract(blocks[1].pos).normal());
		}*/
	}
	
	public static boolean ballCollides(float[] ballCoord, float[] node1Coord,
			float[] node2Coord, float rPallo, float rMato) {
		if (node2Coord[0] == node1Coord[0])
			return !(ballCoord[0] < node1Coord[0]-rPallo-rMato | ballCoord[0] > node1Coord[0]+rPallo+rMato);
		if (node2Coord[1] == node1Coord[1])
			return !(ballCoord[1] < node1Coord[1]-rPallo-rMato | ballCoord[1] > node1Coord[1]+rPallo+rMato);
		float aa = (node2Coord[0] - node1Coord[0]);
		float bb = (node2Coord[1] - node1Coord[1]);
		//lasketaan suoran k
		float k = bb/aa;
		float a = k * -bb;
		float b = k / -aa;
		float c = -a * node2Coord[0] - b * node2Coord[1];
		
		return (square(a * ballCoord[0] + b * ballCoord[1] + c) / square(a) + square(b) < square(rPallo + rMato));
	}
	
	public static float square(float asd){
		return asd * asd;
	}
}
