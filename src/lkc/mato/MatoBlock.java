package lkc.mato;

import java.io.IOException;

import lkc.Engine;
import lkc.Vector2f;
import lkc.display.TexturedQuad;

public class MatoBlock{
	
	public static int r = 6;
	
	public MatoBlock prev;
	public Vector2f pos,dir,vert1,vert2;
	public TexturedQuad tq;
	
	public MatoBlock(MatoBlock mb, Vector2f po, Vector2f di, boolean draw, MatoBlock last, int color){
		prev = mb;
		pos = po;
		dir = di;	
		vert1 = po.add(di.normal().normalize2().multiply(r));
		vert2 = po.add(di.normal().normalize2().multiply(-r));
		if (mb != null & draw & last != null){
			try {
				tq = new TexturedQuad("/diamond.png", new Integer(color), sortVerts(new Vector2f[]{vert1, vert2, last.vert1, last.vert2}, dir.angle()));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private static short[] sortVerts(Vector2f[] verts, double ang) {
		if (ang > Math.PI/2)
			return new short[] {(short)verts[2].x,(short)verts[2].y,0, (short)verts[0].x,(short)verts[0].y,0, (short)verts[3].x,(short)verts[3].y,0, (short)verts[1].x,(short)verts[1].y,0};
		else 
			return new short[] {(short)verts[1].x,(short)verts[1].y,0, (short)verts[3].x,(short)verts[3].y,0, (short)verts[0].x,(short)verts[0].y,0, (short)verts[2].x,(short)verts[2].y,0};
		}

}
