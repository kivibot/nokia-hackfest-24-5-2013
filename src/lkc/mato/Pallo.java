package lkc.mato;

import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.game.Sprite;

import lkc.Engine;
import lkc.Vector2f;
import lkc.display.DisplayObject;

public class Pallo extends DisplayObject {
	
	float px;
	float py;
	float r;
	static float leftEdgeX = -120, rightEdgeX = 120, goalLeftX = -30, goalRightX = 30, topY = 128, bottomY = -128;
	Vector2f v;
	float f;
	Sprite s;
	
	public Pallo(Image img, float friction) {
		s = new Sprite(img);
		r = img.getWidth()/2;
		f = friction;
	}
	
	public void setVel(Vector2f ve){
		v = ve;
	}
	
	public void setPos(float x, float y){
		px = x;
		py = y;
		s.setPosition((int)(px+120-r), (int)(160-py-r));
	}
	
	public boolean move(){
		px+=v.x;
		py+=v.y;
		s.setPosition((int)(px+120-r), (int)((160-py-r)*(400.0/320.0)));
		if (v.lengthSq() != 0)
			v = v.substract(v.clone().normalize2().multiply(v.lengthSq()<f*f?v.length():f));
		if (px-r<leftEdgeX & v.x<0)
			v.x=-v.x;
		if (px+r>rightEdgeX & v.x>0)
			v.x=-v.x;
		if (py+r>topY & (px<goalLeftX|px>goalRightX) & v.y > 0){
			v.y=-v.y;
		} else if (py+r>topY & !(px>goalLeftX+r&px<goalRightX-r)){
			if (px < 0 & new Vector2f(px,py).distSq(new Vector2f(goalLeftX,topY)) < r*r) {
				v = v.reflect(new Vector2f(px,py).substract(new Vector2f(goalLeftX,topY).normal()));
			} else if (px > 0 & new Vector2f(px,py).distSq(new Vector2f(goalRightX,topY)) < r*r){
				v = v.reflect(new Vector2f(px,py).substract(new Vector2f(goalRightX,topY).normal()));
			} 
		}
		if (py > topY & v.y > 0){
			float asd = Engine.getRandom().nextFloat();
			float ang1 = new Vector2f(goalRightX,topY).substract(new Vector2f(px,py)).angle();
			float ang2 = new Vector2f(goalLeftX,topY).substract(new Vector2f(px,py)).angle();
			float fl = ang1+(ang2-ang1)*asd;
			v = Vector2f.makeVec(fl).multiply(Engine.getRandom().nextFloat()*3+1);
			return true;
		}
		
		if (py-r<bottomY & (px<goalLeftX|px>goalRightX) & v.y < 0){
			v.y=-v.y;
		} else if (py-r<bottomY & !(px>goalLeftX+r&px<goalRightX-r)){
			if (px < 0 & new Vector2f(px,py).distSq(new Vector2f(goalLeftX,bottomY)) < r*r) {
				v = v.reflect(new Vector2f(px,py).substract(new Vector2f(goalLeftX,bottomY).normal()));
			} else if (px > 0 & new Vector2f(px,py).distSq(new Vector2f(goalRightX,bottomY)) < r*r){
				v = v.reflect(new Vector2f(px,py).substract(new Vector2f(goalRightX,bottomY).normal()));
			}
		}
		if (py < bottomY & v.y < 0){
			float asd = Engine.getRandom().nextFloat();
			float ang1 = new Vector2f(goalLeftX,bottomY).substract(new Vector2f(px,py)).angle();
			float ang2 = new Vector2f(goalRightX,bottomY).substract(new Vector2f(px,py)).angle();
			float fl = ang1+(ang2-ang1)*asd;
			v = Vector2f.makeVec(fl).multiply(Engine.getRandom().nextFloat()*3+1);
			return true;
		}
		return false;
	}
	
	public float getPy(){
		return py;
	}

	public void paint(Graphics g) {
		g.setColor(0xFFFFFF);
		g.drawRect((int)(leftEdgeX+120), (int)((160-topY)*(400/320.0)), 240, (int)(256*(400/320.0)));
		g.setColor(0);
		g.drawRect((int)(leftEdgeX+120+90), (int)((160-topY)*(400/320.0)), 60, (int)(256*(400/320.0)));
		s.paint(g);
	}
}
